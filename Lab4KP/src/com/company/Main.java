package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) throws IOException {

        System.out.println("Sentences in UA file:");

        // Full text and all sentences UA
        String filePathUA = "C:\\Users\\38096\\IdeaProjects\\Lab4KP\\textUa.txt";
        String contentUA = "";
        contentUA = new String ( Files.readAllBytes( Paths.get(filePathUA) ) );
        BreakIterator iteratorFullUA = BreakIterator.getSentenceInstance(Locale.US);
        iteratorFullUA.setText(contentUA);
        int startFullUA = iteratorFullUA.first();
        String sentenceUA = "";
        for (int end = iteratorFullUA.next(); end != BreakIterator.DONE; startFullUA = end, end = iteratorFullUA.next()) {
            sentenceUA += contentUA.substring(startFullUA, end);
            if(!Character.isLetterOrDigit(sentenceUA.charAt(sentenceUA.length()-1)))
            {
                System.out.println(sentenceUA + "\n");
                sentenceUA = "";
            }
        }

        System.out.println("Sentences in ENG file:");

        // Full text and all sentences ENG
        String filePathENG = "C:\\Users\\38096\\IdeaProjects\\Lab4KP\\textENG.txt";
        String contentENG = "";
        contentENG = new String ( Files.readAllBytes( Paths.get(filePathENG) ) );
        BreakIterator iteratorFullENG = BreakIterator.getSentenceInstance(Locale.US);
        iteratorFullENG.setText(contentENG);
        int startFullENG = iteratorFullENG.first();
        String sentenceENG = "";
        for (int end = iteratorFullENG.next(); end != BreakIterator.DONE; startFullENG = end, end = iteratorFullENG.next()) {
            sentenceENG += contentENG.substring(startFullENG, end);
            if(!Character.isLetterOrDigit(sentenceENG.charAt(sentenceENG.length()-1)))
            {
                System.out.println(sentenceENG + "\n");
                sentenceENG = "";
            }
        }

        //PREPARATION
        File textUA = new File("textUA.txt");
        File textENG = new File("textENG.txt");

        Scanner scanner1 = new Scanner(textUA);
        int lineNumber = 1;
        String[] linesUA = new String[100];

        for(int i = 0; scanner1.hasNextLine(); i++){
            linesUA[i] = scanner1.nextLine();
        }

        Scanner scanner2 = new Scanner(textENG);
        lineNumber = 1;
        String[] linesENG = new String[100];

        for(int i = 0; scanner2.hasNextLine(); i++){
            linesENG[i] = scanner2.nextLine();
        }

        System.out.println("Single-lined sentences in UA file:");

        //Outputting sentences that are only in one line UA
        BreakIterator iteratorUA = BreakIterator.getSentenceInstance(Locale.forLanguageTag("uk_UA"));
        for(int i = 0; i < linesUA.length; i++) {
            if(linesUA[i] == null) break;
            iteratorUA.setText(linesUA[i]);
            int startUA = iteratorUA.first();
            for (int end = iteratorUA.next(); end != BreakIterator.DONE; startUA = end, end = iteratorUA.next()) {
                var potentialSentence = linesUA[i].substring(startUA, end);
                if(Character.isUpperCase(potentialSentence.charAt(0)) &&
                        !Character.isLetterOrDigit(potentialSentence.charAt(potentialSentence.length()-1))
                )
                {
                    System.out.println(potentialSentence);
                }
            }
        }

        System.out.println("\nSingle-lined sentences in ENG file:");

        //Outputting sentences that are only in one line ENG
        BreakIterator iteratorENG = BreakIterator.getSentenceInstance(Locale.US);
        for(int i = 0; i < linesENG.length; i++) {
            if(linesENG[i] == null) break;
            iteratorENG.setText(linesENG[i]);
            int startENG = iteratorENG.first();
            for (int end = iteratorENG.next(); end != BreakIterator.DONE; startENG = end, end = iteratorENG.next()) {
                var potentialSentence = linesENG[i].substring(startENG, end);
                if(Character.isUpperCase(potentialSentence.charAt(0)) &&
                        !Character.isLetterOrDigit(potentialSentence.charAt(potentialSentence.length()-1))
                )
                {
                    System.out.println(potentialSentence);
                }
            }
        }

        System.out.println("\nLinks in UA file:");

        //Links UA
        //var wordsUA = contentUA.split(" ");
        var wordsUA = splitInWords(contentUA);
        for(int i = 0; i < wordsUA.size(); i++)
        {
            if((wordsUA.get(i).toUpperCase().startsWith("C:") || wordsUA.get(i).toUpperCase().startsWith("D:"))
                    && wordsUA.get(i).contains("\\"))
            {
                System.out.println(wordsUA.get(i));
            }
        }

        System.out.println("\nLinks in ENG file:");

        //Links ENG
//        var wordsENG = contentENG.split(" ");
        var wordsENG = splitInWords(contentENG);
        for(int i = 0; i < wordsENG.size(); i++)
        {
            if((wordsENG.get(i).toUpperCase().startsWith("C:") || wordsENG.get(i).toUpperCase().startsWith("D:"))
                    && wordsENG.get(i).contains("\\"))
            {
                System.out.println(wordsENG.get(i));
            }
        }

    }
    public static ArrayList<String> splitInWords(String str) {
        String delims = " ";
        StringTokenizer st = new StringTokenizer(str, delims);
        ArrayList<String> res = new ArrayList<>();
        while (st.hasMoreTokens()) {
            res.add(st.nextToken());
        }
        return res;
    }
}
