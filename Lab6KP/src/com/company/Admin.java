package com.company;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Admin implements Serializable {

    @Serial
    private static final long serialVersionUID = 2L;

    public List<Record> records;

    public Admin()
    {
        records = new ArrayList<Record>();
    }

    public Admin( List<Record> recs)
    {
        records = recs;
    }
}
