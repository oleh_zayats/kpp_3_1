package com.company;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

public class Record implements Serializable {

    @Serial
    private static final long serialVersionUID = 5L;

    public Abonement user;
    public Book book;
    public Date dateOfTaking;
    public Date predictedDateOfReturn;
    public Date actualDateOfReturn;

    public Record(Abonement a, Book b, Date start, Date end, Date predictedEnd)
    {
        user = a;
        book = b;
        dateOfTaking = start;
        predictedDateOfReturn = predictedEnd;
        actualDateOfReturn = end;
    }
}
