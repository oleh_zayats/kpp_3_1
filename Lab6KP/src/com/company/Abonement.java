package com.company;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

public class Abonement implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public String name;
    public String surname;
    public String fathers_name;
    public String email;

    public Abonement(String n, String s, String f, String e)
    {
        name = n;
        surname = s;
        fathers_name = f;
        email = e;
    }
}
