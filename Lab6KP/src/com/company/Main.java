package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<Book> books = List.of(
                new Book("1-st author", "1-st book", 2000),
                new Book("2-nd author", "2-nd book", 2002),
                new Book("3-rd author", "3-rd book",2008),
                new Book("4-th author", "4-th book",2004),
                new Book("5-th author", "5-th book",2002),
                new Book("6-th author", "6-th book",2001)
        );
        List<Abonement> abonements = List.of(
                new Abonement("Name1", "Surname1", "Father`s name1", "Email1"),
                new Abonement("Name2", "Surname2", "Father`s name2", "Email2"),
                new Abonement("Name3", "Surname3", "Father`s name3", "Email3"),
                new Abonement("Name4", "Surname4", "Father`s name4", "Email4"),
                new Abonement("Name5", "Surname5", "Father`s name5", "Email5"),
                new Abonement("Name6", "Surname6", "Father`s name6", "Email6")
        );
        Admin admin = new Admin(List.of(
                new Record(abonements.get(0) ,books.get(0), new Date(2020, 1, 1),new Date(2020, 11, 2),new Date(2020, 11, 10)),
                new Record(abonements.get(0) ,books.get(2), new Date(2020, 1, 1),null,new Date(2020, 11, 10)),
                new Record(abonements.get(0) ,books.get(3), new Date(2020, 1, 1),new Date(2020, 11, 6),new Date(2020, 11, 10)),
                new Record(abonements.get(1) ,books.get(4), new Date(2020, 1, 1),null,new Date(2020, 11, 10)),
                new Record(abonements.get(1) ,books.get(0), new Date(2020, 1, 1),null,new Date(2020, 11, 10)),
                new Record(abonements.get(2) ,books.get(5), new Date(2020, 1, 1),new Date(2020, 11, 3),new Date(2020, 11, 10)),
                new Record(abonements.get(2) ,books.get(1), new Date(2020, 1, 1),new Date(2020, 11, 9),new Date(2020, 11, 10)),
                new Record(abonements.get(3) ,books.get(3), new Date(2020, 1, 1),new Date(2020, 11, 10),new Date(2020, 11, 10)),
                new Record(abonements.get(4) ,books.get(4), new Date(2020, 1, 1),null,new Date(2020, 11, 10)),
                new Record(abonements.get(4) ,books.get(5), new Date(2020, 1, 1),new Date(2020, 11, 1),new Date(2020, 11, 10)),
                new Record(abonements.get(5) ,books.get(0), new Date(2020, 1, 1),null,new Date(2020, 11, 10))
        ));
        Library lib = new Library();
        lib.books = books;
        lib.abonements = abonements;
        lib.admin = admin;
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Enter number of operation\n 1) Sort books\n 2) List of addresses\n 3) Amount of users with provided author\n 4) Biggest amount of books\n 5) Do mailing\n 6) Print debtors");
            int answer = scanner.nextInt();
            if(answer == 1)
            {
                lib.sortBooksByYear();
            }
            else if(answer == 2)
            {
                var res = lib.createListOfEmailsForMailing();
                for(int i = 0; i < res.size(); i++)
                {
                    System.out.println(res.get(i));
                }
            }
            else if(answer == 3)
            {
                System.out.println( lib.getAmountOfUsersThatTookBooksOfProvidedAuthor("1-st author"));
            }
            else if(answer == 4)
            {
                lib.maxAmountOfBooks();
            }
            else if(answer == 5)
            {
                lib.makeMailing();
            }
            else if(answer == 6)
            {
                lib.makeListOfDebtors();
            }
            else if(answer == 7)
            {
                serializingDemo(lib);
            }
        }

    }
    private static void serializingDemo(Library library) throws IOException, ClassNotFoundException {

        try (FileOutputStream fileOutputStream = new FileOutputStream("yourfile.txt");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(library);
            objectOutputStream.flush();
        }

        Library serializedLibrary;
        try (FileInputStream fileInputStream = new FileInputStream("yourfile.txt");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            serializedLibrary = (Library) objectInputStream.readObject();
        }

        System.out.println(serializedLibrary.books);

    }

}
