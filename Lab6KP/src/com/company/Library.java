package com.company;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Library implements Serializable {

    @Serial
    private static final long serialVersionUID = 4L;

    public List<Book> books;
    public List<Abonement> abonements;
    public Admin admin;
    public Library()
    {
//        books = new ArrayList<Book>();
//        abonements = new ArrayList<Abonement>();
//        admin = new Admin();
    }

    public Library(List<Book> b, List<Abonement> a)
    {
        books = b;
        abonements = a;
    }

    // task1
    public void sortBooksByYear()
    {
        System.out.println("Books before sort:\n\n");
        for(int i = 0; i < books.size(); i++)
        {
            System.out.println(books.get(i).toString());
        }

        books = books.stream().sorted((p1, p2) -> p1.year - p2.year).collect(Collectors.toList());

        System.out.println("\n\nBooks after sort:\n\n");
        for(int i = 0; i < books.size(); i++)
        {
            System.out.println(books.get(i).toString());
        }
    }

    // task2
    public ArrayList<String> createListOfEmailsForMailing()
    {
        ArrayList<String> res =  new ArrayList<>();
        var users = this.getHashMapOfUsers();
        var usersWithMoreThanTwoBooks = users.entrySet().stream().filter(p1 -> p1.getValue() >= 2).toList();
        for(int i = 0; i < usersWithMoreThanTwoBooks.size(); i++)
        {
            res.add(usersWithMoreThanTwoBooks.get(i).getKey().email);
        }

//        for(int i = 0; i < users.size(); i++)
//        {
//            var actualUser = admin.records.get(i).user;
//            if(users.get(actualUser) >= 2)
//            {
//                res.add(actualUser.email);
//            }
//        }

        return res;
    }

    // task3
    public int getAmountOfUsersThatTookBooksOfProvidedAuthor(String auth)
    {
        var result = admin.records.stream().filter(p1 -> Objects.equals(p1.book.author, auth));
        return result.toList().size();
//        ArrayList<Abonement> users = new ArrayList<>();
//        for(int i = 0; i < admin.records.size(); i++)
//        {
//            if(admin.records.get(i).book.author == auth && !users.contains(admin.records.get(i).user))
//            {
//                users.add(admin.records.get(i).user);
//            }
//        }

//        return users.size();
    }

//    public void func()
//    {
//        admin = new Admin(List.of(
//                new Record(new Abonement("123","123","123","123"), new Book("auth1","123", 123), new Date(),new Date(),new Date()),
//                new Record(new Abonement("123","123","123","123"), new Book("auth1","123", 123), new Date(),new Date(),new Date()),
//                new Record(new Abonement("123","123","123","123"), new Book("auth1","123", 123), new Date(),new Date(),new Date()),
//                new Record(new Abonement("123","123","123","123"), new Book("auth2","123", 123), new Date(),new Date(),new Date()),
//                new Record(new Abonement("123","123","123","123"), new Book("auth1","123", 123), new Date(),new Date(),new Date())
//        ));
//        var auth = "auth1";
//        var abc = admin.records.stream().filter(p1 -> Objects.equals(p1.book.author, auth));
//        //for(int i = 0; i < abc.toList().size(); i++)
//            System.out.println(abc.toList().size());
//    }

    // task4
    public void maxAmountOfBooks()
    {
        var users = this.getHashMapOfUsers();
        var maxAmountOfBooks = users.values().stream().mapToInt(v -> v).max();
        System.out.println(maxAmountOfBooks.getAsInt());
    }

    //task5
    public void makeMailing()
    {
        var users = this.getHashMapOfUsers();
        var usersWithMoreThanTwoBooks = users.entrySet().stream().filter(p1 -> p1.getValue() >= 2).toList();
        var usersWithLessThanTwoBooks = users.entrySet().stream().filter(p1 -> p1.getValue() < 2).toList();
        for(int i = 0; i < usersWithMoreThanTwoBooks.size(); i++)
        {
            System.out.println(
                    usersWithMoreThanTwoBooks.get(i).getKey().surname + " " +
                            usersWithMoreThanTwoBooks.get(i).getKey().name + " " +
                            usersWithMoreThanTwoBooks.get(i).getKey().fathers_name +
                            ":  Please don`t forget to return books!"
            );
        }

        for(int i = 0; i < usersWithLessThanTwoBooks.size(); i++)
        {
            System.out.println(
                    usersWithLessThanTwoBooks.get(i).getKey().surname + " " +
                            usersWithLessThanTwoBooks.get(i).getKey().name + " " +
                            usersWithLessThanTwoBooks.get(i).getKey().fathers_name +
                            ":  There are such a good new books in our library! Come and enjoy them!!!"
            );
        }
    }


    //task6
    public void makeListOfDebtors()
    {
        ArrayList<Record> recs = new ArrayList<>();
        for(int i = 0; i < admin.records.size(); i++) {
            if(admin.records.get(i).actualDateOfReturn == null)
            {
                recs.add(admin.records.get(i));
            }
        }
        var result = admin.records.stream().filter(p1 -> p1.actualDateOfReturn == null).toList();
        for(int i = 0; i < result.size(); i++)
        {
            Date date1 = new Date(2020, 11, 5);

            // creating the date 2 with sample input date.
            Date date2 = admin.records.get(i).predictedDateOfReturn;

            // getting milliseconds for both dates
            long date1InMs = date1.getTime();
            long date2InMs = date2.getTime();

            // getting the diff between two dates.
            long timeDiff = 0;
            if(date1InMs > date2InMs) {
                timeDiff = date1InMs - date2InMs;
            } else {
                timeDiff = date2InMs - date1InMs;
            }

            // converting diff into days
            int daysDiff = (int) (timeDiff / (1000 * 60 * 60* 24));


            System.out.println(result.get(i).user.surname + " " +
                    result.get(i).user.name + " " +
                    result.get(i).user.surname +
                    ": You should return book " + result.get(i).book.toString()+ " in " + daysDiff + " days"
            );
        }
    }











    private HashMap<Abonement,Integer> getHashMapOfUsers()
    {
        HashMap<Abonement, Integer> users =  new HashMap<Abonement, Integer>();

        for(int i = 0; i < admin.records.size(); i++)
        {
            var actualUser = admin.records.get(i).user;
            if(!users.containsKey(actualUser))
            {
                users.put(actualUser, 1);
            }
            else
            {
                users.put(actualUser, users.get(actualUser) + 1);
            }
        }
        return users;
    }
}
