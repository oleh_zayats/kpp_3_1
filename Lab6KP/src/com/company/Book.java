package com.company;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

public class Book implements Serializable {

    @Serial
    private static final long serialVersionUID = 3L;

    public String author;
    public String title;
    public int year;

    public Book(String a, String t, int y)
    {
        author = a;
        title = t;
        year = y;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", name='" + title + '\'' +
                ", published=" + year +
                '}';
    }

}
