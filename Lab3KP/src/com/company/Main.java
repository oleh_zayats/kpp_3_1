package com.company;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        var clubs = new ArrayList<FootballClub>();
        clubs.add(new FootballClub("Club1", new Date(), "Trostianets"));
        clubs.add(new FootballClub("Club2", new Date(), "Trostianets"));
        clubs.add(new FootballClub("Club3", new Date(), "Trostianets"));
        clubs.add(new FootballClub("Club4", new Date(), "Trostianets"));
        clubs.add(new FootballClub("Club1", new Date(), "Mykolaiv"));
        clubs.add(new FootballClub("Club2", new Date(), "Mykolaiv"));
        clubs.add(new FootballClub("Club3", new Date(), "Mykolaiv"));
        clubs.add(new FootballClub("Club2", new Date(), "Mykolaiv"));
        clubs.add(new FootballClub("Club1", new Date(), "Lviv"));
        clubs.add(new FootballClub("Club2", new Date(), "Lviv"));
        clubs.add(new FootballClub("Club1", new Date(), "Lviv"));
        clubs.add(new FootballClub("Club4", new Date(), "Lviv"));

        Map<String, List<FootballClub>> mapOfFootballClubs = new HashMap<>();
        for(var club: clubs) {
            if(mapOfFootballClubs.get(club.city) != null)
                mapOfFootballClubs.get(club.city).add(club);
            else {
                mapOfFootballClubs.put(club.city, new ArrayList<FootballClub>());
                mapOfFootballClubs.get(club.city).add(club);
            }
        }

        System.out.println("1:\nEnter n:" );
        int n = sc.nextInt();
        var cities = mapOfFootballClubs.keySet();

        for(int j = 0; j < cities.size(); j++)
        {
            var footballClub = mapOfFootballClubs.get(cities.stream().toList().get(j));
            for(int i = 0; i < n; i++)
            {
                System.out.println(footballClub.get(i).ToString());
            }
        }

        System.out.println("2:\n");
        int counter = 0;

        for(int j = 0; j < cities.size(); j++)
        {
            boolean flag = false;
            var footballClub = mapOfFootballClubs.get(cities.stream().toList().get(j));
            for(int i = 0; i < footballClub.size(); i++)
            {
                if(flag)
                    break;
                var footballClubName = footballClub.get(i).name;
                for(int k = 0; k < footballClub.size(); k++)
                {
                    if( i != k && footballClub.get(k).name == footballClubName)
                    {
                        flag = true;
                        counter++;
                        break;
                    }
                }
            }
        }
        System.out.println("Number of cities: " + counter);

        System.out.println("3:\n");
        try {
            List<String> lines = Files.readAllLines(Paths.get("footballClubs1.txt"));

            List<FootballClub> clubsFromFirstFile = new ArrayList<FootballClub>();
            for (String line : lines){
                clubsFromFirstFile.add(new FootballClub(line));
            }

            List<String> lines1 = Files.readAllLines(Paths.get("footballClubs2.txt"));

            List<FootballClub> clubsFromSecondFile = new ArrayList<FootballClub>();
            for (String line : lines1){
                clubsFromSecondFile.add(new FootballClub(line));
            }

            var resultList = intersection(clubsFromFirstFile,clubsFromSecondFile);

            Map<String, List<FootballClub>> mapOfFootballClubs1 = new HashMap<>();
            for(var club: resultList) {
                if(mapOfFootballClubs1.get(club.city) != null)
                    mapOfFootballClubs1.get(club.city).add(club);
                else {
                    mapOfFootballClubs1.put(club.city, new ArrayList<FootballClub>());
                }
            }

            var cities1 = mapOfFootballClubs1.keySet();

            for(int j = 0; j < cities1.size(); j++)
            {
                var footballClub = mapOfFootballClubs1.get(cities1.stream().toList().get(j));
                for(int i = 0; i < footballClub.size(); i++)
                {
                    System.out.println(footballClub.get(i).ToString());
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static List<FootballClub> intersection(List<FootballClub> list1, List<FootballClub> list2) {
        List<FootballClub> list = new ArrayList<FootballClub>();
        for (FootballClub t : list1) {
            for (FootballClub t1 : list1) {
                if(t.equals(t1)) {
                    list.add(t);
                    break;
                }
            }
        }
        return list;
    }
}

