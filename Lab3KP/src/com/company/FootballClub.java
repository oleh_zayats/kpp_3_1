package com.company;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class FootballClub {
    public String name;
    public Date dateOfCreation;
    public String city;
    public FootballClub(String name, Date date, String city)
    {
        this.name = name;
        this.dateOfCreation = date;
        this.city = city;
    }
    public FootballClub(String str)
    {
        List<String> props = Arrays.asList(str.split(" "));
        name = props.get(0);
        city = props.get(1);
        try {
            dateOfCreation = new SimpleDateFormat("dd/MM/yyyy").parse(props.get(2));
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String ToString()
    {
        return "Club name: " + this.name + "\nCity: " + this.city + "\t\tDate of creation: " + this.dateOfCreation.toString() + "\n";
    }

    @Override
    public boolean equals(Object o) {
        FootballClub second = (FootballClub) o;
        if(this.name == second.name && this.dateOfCreation == second.dateOfCreation && this.city == second.city)
            return true;
        return false;
    }
}
