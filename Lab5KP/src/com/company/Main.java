package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        String filePathENG = "C:\\Users\\38096\\IdeaProjects\\Lab5KP\\textENG.txt";
        String contentENG;
        contentENG = new String ( Files.readAllBytes( Paths.get(filePathENG) ) );
        System.out.println(contentENG);


        // The substituted value will be contained in the result variable
        String result = function(contentENG);
        System.out.println("\n\n\n" + result);
    }

    public static String function(String text)
    {
        String result = text.replaceAll("( [tT]he | [aA] | [oO]r | [aA]re | [oO]n | [iI]n | [oO]ut )", "");
        return result;
    }
}
