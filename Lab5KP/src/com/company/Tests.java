package com.company;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

public class Tests {
    @Test
    void testWithEmptyString() {
        var test_string = "";
        var expectedArray = "";
        var actualResult = Main.function(test_string);
        assertEquals(expectedArray, actualResult);
    }

    @Test
    void testWithNoMatches() {
        var test_string = "good good day";
        var expectedString = "good good day";
        var actualResult = Main.function(test_string);
        assertEquals(expectedString, actualResult);
    }

    @Test
    void testWithMatches() {
        var test_string = " the good day";
        var expectedString = "good day";

        var actualResult = Main.function(test_string);
        assertEquals(expectedString, actualResult);
    }
}
