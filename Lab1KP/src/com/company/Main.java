package com.company;
import java.math.BigInteger;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner numb = new Scanner(System.in);
        System.out.println("Enter size of the row:");

        int n = numb.nextInt();
        System.out.println("Your row:");
        if(n <= 15) {
            ArrayList<Fraction> list = new ArrayList();
            for (int i = 1; i <= n; i++) {
                list.add(new Fraction(1 + i, 3 + i * i).reduce());
                System.out.println(list.get(i - 1).numerator + "/" + list.get(i - 1).denominator);
            }
            IFractionMath<Fraction> math = new FractionMath();
            Fraction res = math.Addition(list);
            System.out.println("Result:");
            System.out.println(res.numerator + "/" + res.denominator);
        }
        else{
            ArrayList<BigFraction> list = new ArrayList();
            for (int i = 1; i <= n; i++) {
                list.add(new BigFraction(BigInteger.valueOf(1 + i), BigInteger.valueOf(3 + i * i)).reduce());
                System.out.println(list.get(i - 1).numerator + "/" + list.get(i - 1).denominator);
            }
            IFractionMath<BigFraction> math = new BigFractionMath();
            BigFraction res = math.Addition(list);
            System.out.println("Result:");
            System.out.println(res.numerator + "/" + res.denominator);
        }
        numb.close();
    }
}