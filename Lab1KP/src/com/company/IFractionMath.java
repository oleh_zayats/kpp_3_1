package com.company;

import java.util.ArrayList;

public interface IFractionMath<T> {
    T Addition(ArrayList<T> fractionList);
    T Subtraction(ArrayList<T> fractionList);
    T Multiplication(ArrayList<T> fractionList);
    T Division(ArrayList<T> fractionList);
}
