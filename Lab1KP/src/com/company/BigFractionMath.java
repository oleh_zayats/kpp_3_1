package com.company;

import java.util.ArrayList;

public class BigFractionMath implements IFractionMath<BigFraction> {

    public BigFraction Addition(ArrayList<BigFraction> fractionList) {

        BigFraction result = new BigFraction(fractionList.get(0).numerator, fractionList.get(0).denominator);

        for (int i = 1; i < fractionList.size(); i++) {
            result.numerator = fractionList.get(i).numerator.multiply( result.denominator).add( result.numerator.multiply( fractionList.get(i).denominator));
            result.denominator = fractionList.get(i).denominator.multiply( result.denominator);
            result.reduce();
        }

        return result;
    }

    @Override
    public BigFraction Subtraction(ArrayList<BigFraction> fractionList) {
        return null;
    }

    @Override
    public BigFraction Multiplication(ArrayList<BigFraction> fractionList) {
        return null;
    }

    @Override
    public BigFraction Division(ArrayList<BigFraction> fractionList) {
        return null;
    }
}
