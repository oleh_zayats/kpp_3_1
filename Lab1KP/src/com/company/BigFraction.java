package com.company;

import java.math.BigInteger;

public class BigFraction {
    public BigInteger numerator;
    public BigInteger denominator;


    public BigFraction(BigInteger num, BigInteger den) {
        numerator = num;
        denominator = den;
    }

    public BigFraction reduce() {
        BigInteger res = denominator.gcd(numerator);
        denominator = denominator.divide(res);
        numerator = numerator.divide(res);
        return this;
    }
}
