package com.company;

public class Fraction {
    public int numerator;
    public int denominator;

    public Fraction(int num, int den) {
        numerator = num;
        denominator = den;
    }

    public Fraction reduce() {
        int res;
        int number1 = denominator;
        int number2 = numerator;
        do {
            res = number1 % number2;
            number1 = number2;
            number2 = res;
        } while (res != 0);
        denominator /= number1;
        numerator /= number1;
        return this;
    }
}
