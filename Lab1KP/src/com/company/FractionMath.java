package com.company;

import java.util.ArrayList;

public class FractionMath implements IFractionMath<Fraction> {
    @Override
    public Fraction Addition(ArrayList<Fraction> fractionList) {
        Fraction result = new Fraction(fractionList.get(0).numerator,fractionList.get(0).denominator);

        for(int i = 1; i < fractionList.size(); i++) {
            result.numerator = fractionList.get(i).numerator * result.denominator + result.numerator * fractionList.get(i).denominator;
            result.denominator = fractionList.get(i).denominator * result.denominator;
            result.reduce();
        }

        return result;
    }

    @Override
    public Fraction Subtraction(ArrayList<Fraction> fractionList) {
        return null;
    }

    @Override
    public Fraction Multiplication(ArrayList<Fraction> fractionList) {
        return null;
    }

    @Override
    public Fraction Division(ArrayList<Fraction> fractionList) {
        return null;
    }
}
