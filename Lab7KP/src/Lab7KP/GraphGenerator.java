package Lab7KP;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GraphGenerator {
    static public List<Edge> Generate(int pointCount, int edgeCount) throws Exception {
        if (edgeCount < pointCount)
            throw new Exception("Edge count must be bigger then point count");

        ArrayList<Edge> edges = new ArrayList<>();
        Random r = new Random();

        for (int i = 0; i < pointCount - 1; i++){
            edges.add(new Edge(i, i+1, 20 + r.nextInt(10)));
        }
        for (int i = edges.size(); i < edgeCount; i++){
            AddEdge(edges,pointCount,r);
        }

        return edges;
    }

    static void AddEdge(ArrayList<Edge> edges,int points, Random r){
        var e = new Edge(r.nextInt(points), r.nextInt(points), 1 + r.nextInt(19));
        if(edges.stream().anyMatch(edge -> edge.a == e.a && edge.b == e.b))
            AddEdge(edges,points,r);
        else
            edges.add(e);
    }
}
