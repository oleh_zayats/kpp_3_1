package Lab7KP;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class PrimmsThread extends Thread {
    public void setResult(ArrayList<Edge> result) {
        this.result = result;
    }

    public HashSet<Integer> getCheckedPoints() {
        return checkedPoints;
    }

    public void setCheckedPoints(HashSet<Integer> checkedPoints) {
        this.checkedPoints = checkedPoints;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
        try {
            points = new ArrayList<>();
            for (int i = start; i < end; i++){
                points.add(i);
            }
        }
        catch (Exception e){}
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
        try {
            points = new ArrayList<>();
            for (int i = start; i < end; i++){
                points.add(i);
            }
        }
        catch (Exception e){}
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public Object getLock() {
        return lock;
    }

    public void setLock(Object lock) {
        this.lock = lock;
    }

    public void setLock1(Object lock1) {
        this.lock1 = lock1;
    }

    public void setAllEdges(List<Edge> allEdges) {
        this.allEdges = allEdges;
    }

    int start;
    int end;
    List<Edge> edges;
    List<Edge> allEdges;
    ArrayList<Integer> points;
    volatile HashSet<Integer> checkedPoints;
    volatile ArrayList<Edge> result;
    volatile Object lock;
    volatile Object lock1;

    @Override
    public void run() {
        while (edges.size() != 0) {
            var optionalEdge = edges.stream().filter(edge_ ->
                    (checkedPoints.contains(edge_.getA()) && !checkedPoints.contains(edge_.getB()))
                    || (!checkedPoints.contains(edge_.getA()) && checkedPoints.contains(edge_.getB())))
                    .min(Comparator.comparingInt(Edge::getDistance));
            Edge edge = new Edge((int)Thread.currentThread().getId(), 0, Integer.MAX_VALUE);
            if (optionalEdge.isPresent()) {
                edge = optionalEdge.get();
            }
            result.add(edge);
            synchronized (lock1){
                lock1.notify();
            }
            synchronized (lock) {
                while (result.contains(edge)){
                    try {
                        lock.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
