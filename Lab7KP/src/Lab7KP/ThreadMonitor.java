package Lab7KP;

public class ThreadMonitor {
    public ThreadMonitor(Thread thread){
        this.thread = thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    Thread thread;

    public String GetState(){
        StringBuilder builder = new StringBuilder();
        builder.append("Thread: name: ");
        builder.append(thread.getName());
        builder.append(" state: ");
        builder.append(thread.getState());
        builder.append(" priority: ");
        builder.append(thread.getPriority());
        builder.append(" isAlive: ");
        builder.append(thread.isAlive());
        builder.append("\n");
        return builder.toString();
    }
}
