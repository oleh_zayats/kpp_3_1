package Lab7KP;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class PrimmsAlgorithm2 {
    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    List<Edge> edges;
    int points;
    List<Edge> tree;
    volatile HashSet<Integer> checkedPoints = new HashSet<>();
    ArrayList<Edge> ThreadResults;
    final Object lock = new Object();
    final Object lock1 = new Object();
    volatile List<List<Edge>> threadEdges = new ArrayList<>();
    public List<Edge> MakeTree(int startPoint, int threadCount) throws Exception {
        if(startPoint<0 || startPoint>=points)
            throw new Exception("start point must be in range 0 to points count");

        checkedPoints.add(startPoint);
        ThreadResults = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(threadCount);
        tree = new ArrayList<>();
        var threads = new ArrayList<Callable<Edge>>();
        for(int i = 0; i < threadCount; i++) {
            var list = new ArrayList<Edge>();
            for (int j = i * edges.size() / threadCount; j < (i + 1) * edges.size() / threadCount; j++) {
                list.add(edges.get(j));
            }
            threadEdges.add(list);
            int finalI = i;
            threads.add(()->ThreadMethod(finalI));
        }
        while (checkedPoints.size() != points) {
            ThreadResults.addAll(executor.invokeAll(threads).stream().map(edgeFuture -> {
                try {
                    return edgeFuture.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                return null;
            }).collect(Collectors.toList()));

            var result = ThreadResults.stream()
                    .min(Comparator.comparingInt(Edge::getDistance)).get();
            checkedPoints.add(result.getA());
            checkedPoints.add(result.getB());
            tree.add(result);
            threadEdges.forEach(list -> list.remove(result));
            ThreadResults.clear();
        }
        return tree;
    }
    private Edge ThreadMethod(int threadNum){
        var optionalEdge = threadEdges.get(threadNum).stream().filter(edge_ ->
                        (checkedPoints.contains(edge_.getA()) && !checkedPoints.contains(edge_.getB()))
                                || (!checkedPoints.contains(edge_.getA()) && checkedPoints.contains(edge_.getB())))
                .min(Comparator.comparingInt(Edge::getDistance));
        Edge edge = new Edge((int)Thread.currentThread().getId(), 0, Integer.MAX_VALUE);
        if (optionalEdge.isPresent()) {
            edge = optionalEdge.get();
        }
        return edge;
    }
}
