package Lab7KP;

import java.util.*;

public class PrimmsAlgorithm {
    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    List<Edge> edges;
    int points;
    List<Edge> tree;
    volatile HashSet<Integer> checkedPoints = new HashSet<>();
    volatile ArrayList<Edge> ThreadResults;
    final Object lock = new Object();
    final Object lock1 = new Object();
    volatile List<List<Edge>> threadEdges = new ArrayList<>();

    public List<PrimmsThread> getThreads() {

        return threads;
    }

    List<PrimmsThread> threads = new ArrayList<>();

    public List<Edge> MakeTree(int startPoint, int threadCount, Object mainThreadLock) throws Exception {
        if(startPoint<0 || startPoint>=points)
            throw new Exception("start point must be in range 0 to points count");

        checkedPoints.add(startPoint);
        ThreadResults = new ArrayList<>();
        for(int i = 0; i < threadCount; i++){
            var t = new PrimmsThread();
            var list = new ArrayList<Edge>();
            for (int j = i * edges.size() / threadCount; j < (i + 1) * edges.size() / threadCount; j++){
                list.add(edges.get(j));
            }
            threadEdges.add(list);
            t.setStart(0);
            t.setEnd(points);
            t.setEdges(threadEdges.get(i));
            t.setResult(ThreadResults);
            t.setCheckedPoints(checkedPoints);
            t.setLock(lock);
            t.setLock1(lock1);
            t.setAllEdges(edges);
            t.start();
            threads.add(t);
        }
        synchronized (mainThreadLock){
            mainThreadLock.notify();
        }
        tree = new ArrayList<>();
        while (checkedPoints.size() != points) {
            synchronized (lock1) {
                while (ThreadResults.size() != threads.stream().filter(thread->thread.getState() != Thread.State.TERMINATED).count()) {
                    try {
                        lock1.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            synchronized (lock) {
                var result = ThreadResults.stream()
                        .min(Comparator.comparingInt(Edge::getDistance)).get();
                checkedPoints.add(result.getA());
                checkedPoints.add(result.getB());
                tree.add(result);
                threadEdges.forEach(list->list.remove(result));
                ThreadResults.clear();
                lock.notifyAll();
            }
        }
        threads.forEach(thread->thread.stop());
        return tree;
    }
}
