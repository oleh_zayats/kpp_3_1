package Lab7KP;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws Exception {

        JFrame f = new JFrame();
        f.setTitle("Lab7 By Oleh Zaiats");
        f.setBounds(500, 250, 100, 300);
        JTextArea jTextArea = new JTextArea(" ");
        jTextArea.setBounds(10, 25, 500, 200);
        jTextArea.setSize(600, 600);
        f.add(jTextArea);
        f.setBackground(Color.WHITE);
        jTextArea.getScrollableTracksViewportHeight();
        f.setSize(600, 600);
        f.setLayout(null);
        f.setVisible(true);
        var edges = GraphGenerator.Generate(1000, 2000);
        var primsAlg = new PrimmsAlgorithm();
        primsAlg.setEdges(edges);
        primsAlg.setPoints(1000);
        Object lock = new Object();
        int threadCount = 4;
        CompletableFuture<List<Edge>> tree = CompletableFuture.supplyAsync(()-> {
            try {
                return primsAlg.MakeTree(6, threadCount, lock);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        });
        synchronized (lock){
            while (primsAlg.getThreads().size() != threadCount){
                try{
                    lock.wait();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        var threadMonitors = primsAlg.getThreads().stream().map(thread->new ThreadMonitor(thread)).collect(Collectors.toList());
        var threadMonitorsTasks = new ArrayList<TimerTask>();
        threadMonitorsTasks.add(new TimerTask() {
                                    @Override
                                    public void run() {
                                        if (jTextArea.getLineCount()>=threadCount)
                                            jTextArea.setText("");
                                    }
                                });
        threadMonitors.forEach(monitor -> threadMonitorsTasks.add(new TimerTask() {
                                                                              @Override
                                                                              public void run() {
                                                                                  jTextArea.append(monitor.GetState());
                                                                              }
                                                                          }));
        Timer t = new Timer();
        threadMonitorsTasks.forEach(task->t.scheduleAtFixedRate((TimerTask) task,0,1000));
        tree.get().forEach(edge -> System.out.println(edge.toString()));
    }
}
