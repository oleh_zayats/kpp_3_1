package Lab7KP;

public class Edge {
    int a;
    int b;
    int distance;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String toString() {
        return "("+ a + ", " + b + ") " +distance;
    }

    public Edge(int a, int b, int distance) {
        this.a = a;
        this.b = b;
        this.distance = distance;
    }

}
