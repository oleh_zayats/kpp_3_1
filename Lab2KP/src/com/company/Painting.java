package com.company;

public class Painting extends Unit{
    public Difficulty difficulty;
    public Painting(String t, int p, int a, int q, Category c, Difficulty d) {
        super(t, p, a, q, c);
        difficulty = d;
    }
}
