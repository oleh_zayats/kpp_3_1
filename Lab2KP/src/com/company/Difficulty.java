package com.company;

public enum Difficulty {
    easy,
    medium,
    difficult
}
