package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UnitManager {
    static public  class Sorter {
        static public List<Unit> SortByPrice(List<Unit> units, Order o){
            var result = new ArrayList<Unit>(units);
            Collections.sort(result);
            if(o.equals(Order.ascending)) {
                return result;
            }
            result.sort(new Comparator<Unit>() {
                @Override
                public int compare(Unit o1, Unit o2) {
                    return o2.compareTo(o1);
                }
            });
            return result;
//            var reverseResult = new ArrayList<Unit>();
//            for (int i = result.size() - 1; i >= 0; i--) {
//                reverseResult.add(result.get(i));
//            }
//            return reverseResult;
        }

        static public List<Unit> SortByAmountOfPages(List<Unit> units, Order o ){
            var result = new ArrayList<Unit>(units);
            result.sort(Comparator.comparing(w -> w.AmountOfPages));
            if(o.equals(Order.ascending)) {
                return result;
            }
            var reverseResult = new ArrayList<Unit>();
            for (int i = result.size() - 1; i >= 0; i--) {
                reverseResult.add(result.get(i));
            }
            return reverseResult;
        }

        static public List<Unit> SortByQuantityInStorage(List<Unit> units, Order o){
            var result = new ArrayList<Unit>(units);
            result.sort(Comparator.comparing(w -> w.quantityInStorage));
            if(o.equals(Order.ascending)) {
                return result;
            }
            var reverseResult = new ArrayList<Unit>();
            for (int i = result.size() - 1; i >= 0; i--) {
                reverseResult.add(result.get(i));
            }
            return reverseResult;
        }

        static public List<Unit> FindByCategory(List<Unit> units, Category c)
        {
            var result = new ArrayList<Unit>(units);
            for(int i = 0; i < units.size(); i++)
            {
                if(units.get(i).category == c)
                {
                    result.add(units.get(i));
                }
            }
            return result;
        }
    }
    public class Sort{
        static public List<Unit> SortByCategory(List<Unit> units, Order o){
            var result = new ArrayList<Unit>(units);
            result.sort(Comparator.comparing(w -> w.category));

            if(o.equals(Order.ascending)) {
                return result;
            }
            var reverseResult = new ArrayList<Unit>();
            for (int i = result.size() - 1; i >= 0; i--) {
                reverseResult.add(result.get(i));
            }
            return reverseResult;
        }
    }
}
