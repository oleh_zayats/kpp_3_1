package com.company;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Unit implements Comparable<Unit> {
    public String title;
    public Integer price;
    public Integer AmountOfPages;
    public Integer quantityInStorage;
    public Category category;

    public Unit(String t, int p, int a, int q, Category c)
    {
        title = t;
        price = p;
        AmountOfPages = a;
        quantityInStorage = q;
        category = c;
    }

    @Override
    public int compareTo(Unit o) {
        return this.price.compareTo(o.price);
    }
}

