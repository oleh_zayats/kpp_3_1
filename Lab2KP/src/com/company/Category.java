package com.company;

public enum Category {
    forChildren,
    forTeenagers,
    forAdults
}
