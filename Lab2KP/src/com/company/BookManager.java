package com.company;

import java.util.ArrayList;
import java.util.List;

public class BookManager {
    static public List<Book> SortByAuthor(List<Book> units, Order o){
        var result = new ArrayList<Book>(units);
        result.sort((w1, w2) -> w1.author.compareTo(w2.author));
        if(o.equals(Order.ascending)) {
            return result;
        }
        var reverseResult = new ArrayList<Book>();
        for (int i = result.size() - 1; i >= 0; i--) {
            reverseResult.add(result.get(i));
        }
       return reverseResult;
    }

    static public List<Book> FindByAuthor(List<Book> units, String c)
    {
        var result = new ArrayList<Book>();
        for(int i = 0; i < units.size(); i++)
        {
            if(units.get(i).author == c)
            {
                result.add(units.get(i));
            }
        }
        return result;
    }
}
