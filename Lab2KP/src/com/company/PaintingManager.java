package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public  class PaintingManager {
     static public List<Painting> SortByDifficulty(List<Painting> units, Order o){
        var result = new ArrayList<Painting>(units);
        result.sort(Comparator.comparing(w -> w.difficulty));
        if(o.equals(Order.ascending)) {
            return result;
        }
        var reverseResult = new ArrayList<Painting>();
        for (int i = result.size() - 1; i >= 0; i--) {
            reverseResult.add(result.get(i));
        }
        return reverseResult;
    }

    static public List<Painting> FindByDifficulty(List<Painting> units, Difficulty c)
    {
        var result = new ArrayList<Painting>();
        for(int i = 0; i < units.size(); i++)
        {
            if(units.get(i).difficulty == c)
            {
                result.add(units.get(i));
            }
        }
        return result;
    }
}
