package com.company;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("depth", 100, 250, 10, Category.forAdults, "john"));
        bookList.add(new Book("history", 300, 454, 100, Category.forChildren, "poem"));
        bookList.add(new Book("police", 450, 52, 1000, Category.forTeenagers, "dad"));
        bookList.add(new Book("significance", 150, 231, 50, Category.forTeenagers, "fortune"));
        bookList.add(new Book("currency", 175, 812, 3, Category.forChildren, "imagination"));
        bookList.add(new Book("tennis", 254, 1123, 4, Category.forAdults, "village"));
        bookList.add(new Book("problem", 150, 142, 8, Category.forAdults, "manufacturer"));
        bookList.add(new Book("restaurant", 330, 532, 11, Category.forChildren, "idea"));
        bookList.add(new Book("opportunity", 174, 214, 12, Category.forChildren, "math"));
        bookList.add(new Book("desk", 211, 214, 10, Category.forAdults, "mom"));

        List<Painting> paintingList = new ArrayList<>();
        paintingList.add(new Painting("appointment", 54, 1, 110, Category.forTeenagers, Difficulty.easy));
        paintingList.add(new Painting("history", 33, 1, 120, Category.forAdults, Difficulty.medium));
        paintingList.add(new Painting("police", 23, 1, 10, Category.forChildren, Difficulty.difficult));
        paintingList.add(new Painting("significance", 100, 1, 31, Category.forAdults, Difficulty.easy));
        paintingList.add(new Painting("currency", 11, 1, 22, Category.forChildren, Difficulty.medium));
        paintingList.add(new Painting("tennis", 99, 1, 32, Category.forChildren, Difficulty.difficult));
        paintingList.add(new Painting("problem", 100, 1, 39, Category.forAdults, Difficulty.difficult));
        paintingList.add(new Painting("restaurant", 312, 1, 22, Category.forChildren, Difficulty.medium));
        paintingList.add(new Painting("opportunity", 32, 1, 44, Category.forAdults, Difficulty.easy));
        paintingList.add(new Painting("desk", 100, 1, 73, Category.forTeenagers, Difficulty.easy));

        List<Unit> unitListOfBooks = new ArrayList<>(bookList);
        List<Unit> unitListOfPaintings = new ArrayList<>(paintingList);

        boolean leave = false;
        while (!leave) {
            System.out.println("Choose action:");
            System.out.println("0 => To exit");
            System.out.println("1 => Sort books by author ascending");
            System.out.println("2 => Sort books by author descending");
            System.out.println("3 => Sort books by price ascending");
            System.out.println("4 => Sort books by price descending");
            System.out.println("5 => Sort paintings by difficulty ascending");
            System.out.println("6 => Sort paintings by difficulty descending");
            System.out.println("7 => Sort paintings by category ascending");
            System.out.println("8 => Sort paintings by category descending");
            System.out.println("9 => Find book by author (john)");
            System.out.println("10 => Find painting by difficulty(easy)");
            String action = in.nextLine();
            switch (action) {
                case "0" -> leave = true;
                case "1" -> {
                    var res = BookManager.SortByAuthor(bookList, Order.ascending);
                    for (Book book : res) {
                        System.out.println(book.author);
                    }
                }
                case "2" -> {
                    var res = BookManager.SortByAuthor(bookList, Order.descending);
                    for (Book book : res) {
                        System.out.println(book.author);
                    }
                }
                case "3" -> {
                    var res = UnitManager.Sorter.SortByPrice(unitListOfBooks, Order.ascending);
                    for (Unit book : res) {
                        System.out.println(book.price);
                    }
                }
                case "4" -> {
                    var res = UnitManager.Sorter.SortByPrice(unitListOfBooks, Order.descending);
                    for (Unit book : res) {
                        System.out.println(book.price);
                    }
                }
                case "5" -> {
                    var res = PaintingManager.SortByDifficulty(paintingList, Order.ascending);
                    for (Painting painting : res) {
                        System.out.println(painting.difficulty);
                    }
                }
                case "6" -> {
                    var res = PaintingManager.SortByDifficulty(paintingList, Order.descending);
                    for (Painting painting : res) {
                        System.out.println(painting.difficulty);
                    }
                }
                case "7" -> {
                    var res = UnitManager.Sort.SortByCategory(unitListOfPaintings, Order.ascending);
                    for (Unit painting : res) {
                        System.out.println(painting.category);
                    }
                }
                case "8" -> {
                    var res = UnitManager.Sort.SortByCategory(unitListOfPaintings, Order.descending);
                    for (Unit painting : res) {
                        System.out.println(painting.category);
                    }
                }
                case "9" -> {
                    var res = BookManager.FindByAuthor(bookList, "john");
                    for (Book book : res) {
                        System.out.println(book.author);
                    }
                }
                case "10" -> {
                    var res = PaintingManager.FindByDifficulty(paintingList, Difficulty.easy);
                    for (Painting painting : res) {
                        System.out.println(painting.difficulty);
                    }
                }

            }
        }
    }
}
